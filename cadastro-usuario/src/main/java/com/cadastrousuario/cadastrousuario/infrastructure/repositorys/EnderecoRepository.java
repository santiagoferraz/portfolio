package com.cadastrousuario.cadastrousuario.infrastructure.repositorys;

import com.cadastrousuario.cadastrousuario.infrastructure.entities.EnderecoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnderecoRepository extends JpaRepository<EnderecoEntity, Long> {
}
