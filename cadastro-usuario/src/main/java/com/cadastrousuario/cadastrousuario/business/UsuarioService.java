package com.cadastrousuario.cadastrousuario.business;

import com.cadastrousuario.cadastrousuario.api.converter.UsuarioConverter;
import com.cadastrousuario.cadastrousuario.api.request.UsuarioRequestDTO;
import com.cadastrousuario.cadastrousuario.api.response.UsuarioResponseDTO;
import com.cadastrousuario.cadastrousuario.infrastructure.entities.UsuarioEntity;
import com.cadastrousuario.cadastrousuario.infrastructure.repositorys.UsuarioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UsuarioService {

    private final UsuarioRepository usuarioRepository;
    private final UsuarioConverter usuarioConverter;

    public UsuarioEntity salvarUsuario(UsuarioEntity usuarioEntity) {
        return usuarioRepository.saveAndFlush(usuarioEntity);
    }

    public UsuarioResponseDTO gravarUsuario(UsuarioRequestDTO usuarioRequestDTO) {
        UsuarioEntity usuarioEntity = usuarioRepository.findByEmail(usuarioRequestDTO.getEmail());
        if (!Objects.isNull(usuarioEntity)) {
            usuarioRequestDTO.setId(usuarioEntity.getId());
        }
        usuarioEntity = usuarioConverter.paraUsuarioEntity(usuarioRequestDTO);
        usuarioEntity = salvarUsuario(usuarioEntity);
        return usuarioConverter.paraUsuarioResponseDTO(usuarioEntity);
    }

    public UsuarioResponseDTO buscarUsuarioPorEmail(String email) {
        return usuarioConverter.paraUsuarioResponseDTO(usuarioRepository.findByEmail(email));
    }

    public void deleteUsuarioPorEmail(String email) {
        usuarioRepository.deleteByEmail(email);
    }

}
