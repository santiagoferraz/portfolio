package com.cadastrousuario.cadastrousuario.api.converter;

import com.cadastrousuario.cadastrousuario.api.request.EnderecoRequestDTO;
import com.cadastrousuario.cadastrousuario.api.request.UsuarioRequestDTO;
import com.cadastrousuario.cadastrousuario.api.response.EnderecoResponseDTO;
import com.cadastrousuario.cadastrousuario.api.response.UsuarioResponseDTO;
import com.cadastrousuario.cadastrousuario.infrastructure.entities.EnderecoEntity;
import com.cadastrousuario.cadastrousuario.infrastructure.entities.UsuarioEntity;
import org.springframework.stereotype.Component;

@Component
public class UsuarioConverter {

    public UsuarioEntity paraUsuarioEntity(UsuarioRequestDTO usuarioDTO) {
        return UsuarioEntity.builder()
                .id(usuarioDTO.getId())
                .nome(usuarioDTO.getNome())
                .email(usuarioDTO.getEmail())
                .documento(usuarioDTO.getDocumento())
                .endereco(paraEnderecoEntity(usuarioDTO.getEndereco()))
                .build();
    }

    private EnderecoEntity paraEnderecoEntity(EnderecoRequestDTO enderecoDTO) {
        return EnderecoEntity.builder()
                .rua(enderecoDTO.getRua())
                .numero(enderecoDTO.getNumero())
                .complemento(enderecoDTO.getComplemento())
                .bairro(enderecoDTO.getBairro())
                .cidade(enderecoDTO.getCidade())
                .uf(enderecoDTO.getUf())
                .cep(enderecoDTO.getCep())
                .build();
    }

    public UsuarioResponseDTO paraUsuarioResponseDTO(UsuarioEntity usuarioEntity) {
        return UsuarioResponseDTO.builder()
                .id(usuarioEntity.getId())
                .nome(usuarioEntity.getNome())
                .email(usuarioEntity.getEmail())
                .documento(usuarioEntity.getDocumento())
                .endereco(paraEnderecoResponseDTO(usuarioEntity.getEndereco()))
                .build();
    }

    private EnderecoResponseDTO paraEnderecoResponseDTO(EnderecoEntity enderecoEntity) {
        return EnderecoResponseDTO.builder()
                .rua(enderecoEntity.getRua())
                .numero(enderecoEntity.getNumero())
                .complemento(enderecoEntity.getComplemento())
                .bairro(enderecoEntity.getBairro())
                .cidade(enderecoEntity.getCidade())
                .uf(enderecoEntity.getUf())
                .cep(enderecoEntity.getCep())
                .build();
    }
}
