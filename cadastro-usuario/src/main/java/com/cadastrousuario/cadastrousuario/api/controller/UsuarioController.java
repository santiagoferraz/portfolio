package com.cadastrousuario.cadastrousuario.api.controller;

import com.cadastrousuario.cadastrousuario.api.request.UsuarioRequestDTO;
import com.cadastrousuario.cadastrousuario.api.response.UsuarioResponseDTO;
import com.cadastrousuario.cadastrousuario.business.UsuarioService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UsuarioController {

    private final UsuarioService usuarioService;

    @PostMapping()
    public ResponseEntity<UsuarioResponseDTO> salvarUsuario(@RequestBody UsuarioRequestDTO usuarioDTO) {
        return ResponseEntity.ok(usuarioService.gravarUsuario(usuarioDTO));
    }

    @GetMapping()
    public ResponseEntity<UsuarioResponseDTO> buscarUsuarioPorEmail(@RequestParam("email") String email) {
        return ResponseEntity.ok(usuarioService.buscarUsuarioPorEmail(email));
    }

    @DeleteMapping()
    public ResponseEntity<Void> deleteUsuarioPorEmail(@RequestParam("email") String email) {
        usuarioService.deleteUsuarioPorEmail(email);
        return ResponseEntity.accepted().build();
    }

}
