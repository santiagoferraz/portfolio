package com.cadastrousuario.cadastrousuario.api.request;

import com.cadastrousuario.cadastrousuario.infrastructure.entities.EnderecoEntity;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsuarioRequestDTO {
    private Long id;
    private String nome;
    private String email;
    private String documento;
    private EnderecoRequestDTO endereco;
}
