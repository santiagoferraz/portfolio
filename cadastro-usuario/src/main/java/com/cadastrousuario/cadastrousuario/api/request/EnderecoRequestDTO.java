package com.cadastrousuario.cadastrousuario.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EnderecoRequestDTO {
    private String rua;
    private Long numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private String uf;
    private String cep;
}
